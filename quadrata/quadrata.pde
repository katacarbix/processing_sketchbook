Camera camera;

int gridSize = 20;
float gridScale = 5;
float grid[][] = new float[gridSize][gridSize];

void setup(){
	size(1024, 768, P3D);
	frameRate(60);
	smooth(16);
	
	camera = new Camera();
	camera.position = new PVector(50, -92, 175);
	camera.phi = -0.66;

	// for (int x = 0; x < gridSize; x++){
	// 	for (int z = 0; z < gridSize; z++){
	// 		grid[x][z] = 0;
	// 	}
	// }
}

void update(){
	for (int x = 0; x < gridSize; x++){
		for (int z = 0; z < gridSize; z++){
			grid[x][z] = 5*(sin(radians(millis()/10.0 + x*gridScale*2)) + sin(radians(millis()/7.0 + z*gridScale*2)));
		}
	}
}

void draw(){
	// println(frameCount);
	update();
	
	background(0);
	
	for (int x = 0; x < gridSize; x++){
		for (int z = 0; z < gridSize; z++){
			if (x % 2 == 0 && z % 2 == 0) stroke(0, 215, 255); // light blue
			else if (x % 2 == 1 && z % 2 == 1) stroke(255, 0, 191); // pink
			else continue;
			
			if (x+2 < gridSize){
				line( x*gridScale,		grid[x][z],		 z*gridScale,
					 (x+2)*gridScale,	grid[x+2][z],	 z*gridScale);
			}
			if (z+2 < gridSize){
				line( x*gridScale,		grid[x][z],		 z*gridScale,
					  x*gridScale,		grid[x][z+2],	(z+2)*gridScale);
			}
		}
	}
	
	camera.Update(1.0/frameRate);
	// println(camera.position, camera.theta, camera.phi);
	
	// saveFrame("out/###.png");
	// if (radians(millis()/48.0) >= TAU) exit();
}

void keyPressed(){
	if (key == 'x') saveFrame("out/###.png");
	
	camera.HandleKeyPressed();
}

void keyReleased(){
	camera.HandleKeyReleased();
}
