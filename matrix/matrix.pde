int gridX = 60;
int gridY = 60;
boolean[][] matrix = new boolean[gridX][gridY];

int textSize = 20;

void setup(){
	size(1200, 800);
	frameRate(30);
}

void update(){
	// shift rows down
	for (int y = gridY-1; y > 0; y--){
		for (int x = 0; x < gridX; x++){
			matrix[x][y] = matrix[x][y-1];
		}
	}
	
	// generate new top row
	for (int x = 0; x < gridX; x++){
		matrix[x][0] = (random(1) <= 0.05);
	}
}

void draw(){
	// move every 3 frames
	if (frameCount % 3 == 0) update();
	
	background(0);
	
	for (int x = 0; x < gridX; x++){
		for (int y = 0; y < gridY; y++){
			
			int nearestFront = y;
			while (!matrix[x][nearestFront]){
				nearestFront++;
				if (nearestFront >= gridY){
					nearestFront = 999;
					break;
				}
			}
			float frontProximity = max(20-(nearestFront-y), 0)/20.0;
			fill(0,255*frontProximity,0);
			
			char c = (char) random(33,127);
			text(Character.toString(c), x*textSize, (y+1)*textSize);
		}
	}
}
