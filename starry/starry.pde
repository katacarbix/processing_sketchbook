void setup(){
  size(2000,4000); // must be in [resolution] increments
  background(0);
  noStroke();
  
  int resolution = 200;
  int freq = 10; // stars per [resolution]x[resolution] square
  float maxSize = 2;
  
  for (int i = 0; i < height; i += resolution){
    for (int j = 0; j < width; j += resolution){
      for (int k = 0; k < freq; k++){
        float x = random(j, j+resolution);
        float y = random(i, i+resolution);
        float size = random(maxSize);
        
        fill((size/maxSize)*255);
        
        circle(x, y, size);
      }
    }
  }
  
  save("starry.png");
}
