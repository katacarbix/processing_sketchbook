Camera camera;

int vertsPerLayer = 20;
int bufSize = 6*vertsPerLayer;
ArrayList<Layer> vertBuf = new ArrayList<Layer>();

float maxRad = 60.0;
float layerHeight = 3.0;
float layerDeviation = 10.0;

float speed = 0.15;

float timer;
void setup(){
	size(1024, 768, P3D);
	smooth(4);
	
	camera = new Camera();
	camera.position = new PVector(0, 50, 200);

	generate();
	timer = millis();
}

void generate(){
	vertBuf = new ArrayList<Layer>();
	for (int i = 0; i < bufSize; i++){
		float r = radius(i)*sqrt(random(1));
		float t = random(TWO_PI);
		float y = i*layerHeight/3.0 + random(-layerDeviation, layerDeviation);
		
		vertBuf.add(new Layer(r, t, y));
	}
}

float radius(float level){
	return abs(maxRad*sin(level*PI/bufSize));
}

void draw(){
	// println(frameCount);
	float dt = 1.0/frameRate;
	if (millis() >= timer+1500){
		timer = millis();
		generate();
	}
	
	background(80);
	
	ambientLight(130,130,130);
	directionalLight(90,100,90, 1,6,-2);
	lightFalloff(1, 0.001, 0);
	lightSpecular(128,128,128);
	
	shininess(2.0);
	
	fill(255,255,255);
	noStroke();
	
	for (int i = 0; i < vertBuf.size(); i+=vertsPerLayer){
		beginShape(TRIANGLE_STRIP);
		for (int j = 0; j < vertsPerLayer; j++){
			// add vertex for layer
			Layer l = vertBuf.get(i+j);
			float x = l.r*cos(l.t);
			float z = l.r*sin(l.t);
			vertex(x, l.y, z);
			
			// modify layer
			l.t += sin((l.y)*47)*speed*dt;
			l.r = min(radius(i), max(1E-8, l.r+random(-.2,.2)*speed*dt));
		}
		endShape();
	}
	
	camera.Update(dt);
	// println(camera.position, camera.theta, camera.phi);
}

void keyPressed(){
	if (key == 'r'){
		timer = millis();
		generate();
	}
	
	camera.HandleKeyPressed();
}

void keyReleased(){
	camera.HandleKeyReleased();
}

public class Layer {
	public float r;
	public float t;
	public float y;
	
	public Layer(float r, float t, float y){
		this.r = r;
		this.t = t;
		this.y = y;
	}
}
