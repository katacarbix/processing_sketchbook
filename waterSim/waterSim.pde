Camera camera;

// 3D dimensions
float height3D = 500;
float width3D = 500;
float depth = -500;

int n = 40;				// number of cells
float dx = width3D/n;	// width of cell
float dz = depth/n;		// depth of cell
float g = 10.0;			// gravity
float damp = 0.8;		// damping

float h[][] = new float[n][n];		// height
float hu[][] = new float[n][n];		// momentum (x)
float hv[][] = new float[n][n];		// momentum (z)
float dhdt[][] = new float[n][n];	// height (x)
float dhudt[][] = new float[n][n];	// momentum (x)
float dhvdt[][] = new float[n][n];	// momentum (z)

// Midpoint helpers
float h_mid[][] = new float[n][n];		// height
float hu_mid[][] = new float[n][n];		// momentum (x)
float hv_mid[][] = new float[n][n];		// momentum (z)
float dhdt_mid[][] = new float[n][n];	// height
float dhudt_mid[][] = new float[n][n];	// momentum (x)
float dhvdt_mid[][] = new float[n][n];	// momentum (z)

void initialize(){
	for (int i = 0; i < n; i++){
		for (int j = 0; j < n; j++){
			h[i][j] = 50 + 400/(sq(i+j-5)+10);
			hu[i][j] = 0;
			hv[i][j] = 0;
		}
	}
}

void setup(){
	size(1000, 800, P3D);
	surface.setTitle("2D Shallow Water Simulation");
	
	camera = new Camera();
	camera.position = new PVector(250, 0, 430);
	camera.phi = -0.64;
	
	initialize();
}


float dhudx;
float dhvdz;
float dhu2dx;
float dhv2dz;
float dgh2dx;
float dgh2dz;

float dhudx_mid;
float dhvdz_mid;
float dhu2dx_mid;
float dhv2dz_mid;
float dgh2dx_mid;
float dgh2dz_mid;

void update(float dt){
	// compute midpoint heights and momentums
	for (int i = 0; i < n-1; i++){
		for (int j = 0; j < n-1; j++){
			h_mid[i][j] = (h[i+1][j+1]+h[i][j])/2.0;
			
			hu_mid[i][j] = (hu[i+1][j]+hu[i][j])/2.0;
			hv_mid[i][j] = (hv[i][j+1]+hv[i][j])/2.0;
		}
	}
	
	for (int i = 0; i < n-1; i++){
		for (int j = 0; j < n-1; j++){
			// Compute dh/dt in X direction (mid)
			dhudx_mid = (hu[i+1][j] - hu[i][j])/dx;
			// Compute dh/dt in Z direction (mid)
			dhvdz_mid = (hv[i][j+1] - hv[i][j])/dx;
			dhdt_mid[i][j] = -dhudx_mid - dhvdz_mid;
			
			// Compute dhu/dt in X direction (mid)
			dhu2dx_mid = (sq(hu[i+1][j])/h[i+1][j] - sq(hu[i][j])/h[i][j])/dx;
			dgh2dx_mid = g*(sq(h[i+1][j]) - sq(h[i][j]))/dx;
			dhudt_mid[i][j] = -(dhu2dx_mid + .5*dgh2dx_mid);
			// Compute dhu/dt in Z direction (mid)
			dhv2dz_mid = (sq(hv[i][j+1])/h[i][j+1] - sq(hu[i][j])/h[i][j])/dx;
			dgh2dz_mid = g*(sq(h[i][j+1]) - sq(h[i][j]))/dx;
			dhvdt_mid[i][j] = -(dhv2dz_mid + .5*dgh2dz_mid);
		}
	}
	
	// integrate midpoints
	for (int i = 0; i < n-1; i++){
		for (int j = 0; j < n-1; j++){
			h_mid[i][j] += dhdt_mid[i][j] * dt / 2;
			hu_mid[i][j] += dhudt_mid[i][j] * dt / 2;
			hv_mid[i][j] += dhvdt_mid[i][j] * dt / 2;
		}
	}
	
	for (int i = 1; i < n-1; i++){
		for (int j = 1; j < n-1; j++){
			// compute dh/dt in X direction
			dhudx = (hu_mid[i][j] - hu_mid[i-1][j])/dx;
			// compute dh/dt in Z direction
			dhvdz = (hv_mid[i][j] - hv_mid[i][j-1])/dz;
			dhdt[i][j] = -dhudx - dhvdz;
			
			// compute dhu/dt in X direction
			dhu2dx = (sq(hu_mid[i][j])/h_mid[i][j] - sq(hu_mid[i-1][j])/h_mid[i-1][j])/dx;
			dgh2dx = g*(sq(h_mid[i][j]) - sq(h_mid[i-1][j]))/dx;
			dhudt[i][j] = -(dhu2dx + .5*dgh2dx);
			// compute dhu/dt in Z direction
			dhv2dz = (sq(hv_mid[i][j])/h_mid[i][j] - sq(hv_mid[i][j-1])/h_mid[i][j-1])/dz;
			dgh2dz = g*(sq(h_mid[i][j]) - sq(h_mid[i][j-1]))/dz;
			dhvdt[i][j] = -(dhv2dz + .5*dgh2dz);
		}
	}
	
	// integrate heights and momentums
	for (int i = 0; i < n-1; i++){
		for (int j = 0; j < n-1; j++){
			h[i][j] += damp * dhdt[i][j] * dt;
			hu[i][j] += damp * dhudt[i][j] * dt;
			hv[i][j] += damp * dhvdt[i][j] * dt;
		}
	}
	
	// reflective boundary condition
	// h[0] = h[1];
	// h[n-1] = h[n-2];
	// hu[0] = -hu[1];
	// hu[n-1] = -hu[n-2];
}

void draw(){
	background(255);
	noStroke();
	fill(0,127,255);
	
	ambientLight(128, 128, 128);
	directionalLight(255,255,255, -0.5, 1, -0.25);
	lightFalloff(1, 0.001, 0);
	lightSpecular(64,64,64);
	
	// surface of water
	for (int i = 0; i < n-1; i++){
		beginShape(QUAD_STRIP);
		for (int j = 0; j < n; j++){
			float x = i*dx;
			float z = j*dz;
			
			vertex(x, height3D-h[i][j], z);
			vertex(x+dx, height3D-h[i+1][j], z);
		}
		endShape();
	}
	
	// front
	// beginShape(QUAD_STRIP);
	// for (int i = 0; i < n; i++){
	// 	float x = i*dx;
	//
	// 	vertex(x, height3D-h[i], 0);
	// 	vertex(x, height3D, 0);
	// }
	// endShape();
	
	update(1.0/30.0);
	camera.Update(1.0/frameRate);
	// println(camera.position, camera.theta, camera.phi);
}


void keyPressed()
{
  camera.HandleKeyPressed();
}

void keyReleased()
{
  camera.HandleKeyReleased();
}
