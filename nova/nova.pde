import java.util.Date;

int numLines = 50;
PVector[][] line = new PVector[numLines][2];

void generate(){
	for (int i = 0; i < numLines; i++){
		float theta = random(TAU);
		float start = random(10, 50);
		float end   = random(100,250);
		
		line[i][0] = new PVector(
			250 + (start * cos(theta)),
			250 + (start * sin(theta)) );
		line[i][1] = new PVector(
			250 + (end * cos(theta)),
			250 + (end * sin(theta)) );
	}
}

void setup(){
	size(500,500);
	generate();
}

void draw(){
	background(0);
	stroke(255);
	strokeWeight(0.0001);
	
	for (int i = 0; i < numLines; i++){
		line(line[i][0].x, line[i][0].y, line[i][1].x, line[i][1].y);
	}
}

void keyTyped(){
	if (key == 'r'){
		generate();
	}
	if (key == 's'){
		Date d = new Date();
		long current = d.getTime()/1000;
		save(current+".png");
		println("saved file as '"+current+".png'");
	}
	if (key == ESC){
		exit();
	}
}
